import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivingNotificationsComponent } from './receiving-notifications.component';

describe('ReceivingNotificationsComponent', () => {
  let component: ReceivingNotificationsComponent;
  let fixture: ComponentFixture<ReceivingNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceivingNotificationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReceivingNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

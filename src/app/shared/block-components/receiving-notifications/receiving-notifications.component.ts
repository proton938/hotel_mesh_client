import { Component, inject, OnInit } from '@angular/core';
import { EMPTY } from 'rxjs';
import { catchError, delay, map, retry } from 'rxjs/operators';
import { NotificationsStreamService } from '../../rest/notifications-stream/notifications-stream.service';

@Component({
  selector: 'app-receiving-notifications',
  templateUrl: './receiving-notifications.component.html',
  styleUrls: ['./receiving-notifications.component.css']
})
export class ReceivingNotificationsComponent implements OnInit {

  private notificationsStream = inject(NotificationsStreamService);

  notificationBuffer = '';

  ngOnInit(): void {
    this.notificationsStream.receivingNotifications()
      .pipe(
        map((res: string) => this.handleEvent(res)),
        delay(3000),
        retry(Infinity),
        catchError(() => this.handleError()),
      )
      .subscribe();
  }

  private handleEvent(notification: string) {
    console.log(notification);
    if (this.notificationBuffer !== notification) {
      alert(notification);
      this.notificationBuffer = notification;
    }
  }

  private handleError() {
    return EMPTY;
  }

}

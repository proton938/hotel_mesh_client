export interface IClients {
  id: number;
  fio: string;
  phone: string;
  email: string;
  time?: number;
}

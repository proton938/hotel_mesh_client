import { inject, Injectable, NgZone } from '@angular/core';
import { BASIC_SOURCE } from '../../../../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NotificationsStreamService {

  private zone = inject(NgZone);

  receivingNotifications(): Observable<any> {
    return Observable.create(
      (observer: any) => {
        let source = new EventSource(BASIC_SOURCE + '/notification/listen');
        source.onmessage = event => {
          this.zone.run(() => {
            observer.next(event.data);
          })
        };
        source.onerror = event => {
          this.zone.run(() => {
            source.close();
            observer.error(event);
          })
        }
      }
    );
  }

}

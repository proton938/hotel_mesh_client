import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { BASIC_SOURCE } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ClientsStreamService {

  constructor(
    private zone: NgZone,
  ) {}

  getClientsStream(): Observable<any> {
    return Observable.create(
      (observer: any) => {
        let source = new EventSource(BASIC_SOURCE + '/request/allClients');
        source.onmessage = event => {
          this.zone.run(() => {
            observer.next(event.data);
          })
        };
        source.onerror = event => {
          this.zone.run(() => {
            observer.error(event);
          })
        }
      }
    );
  }

}


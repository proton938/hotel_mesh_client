import { Component, NgZone, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ClientsStreamService } from "../../shared/rest/clients-stream.service";
import { Subscription, throwError } from 'rxjs';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  private sub: Subscription | undefined;
  dataBase: any[] = [];

  constructor(
    private zone: NgZone,
    private http: HttpClient,
    private clientsStreamService: ClientsStreamService,
  ) {
  }

  ngOnInit(): void {
    this.sub = this.clientsStreamService.getClientsStream()
      .subscribe(
        (client: string) => this.handleResponseGetClients(client),
        err => throwError(err)
      );
  }

  private handleResponseGetClients(client: string) {
    const parseClient: IClients = JSON.parse(client);
    parseClient.time = new Date().getTime();
    let index: number = this.dataBase.findIndex((item: any) => item['id'] === parseClient.id);
    const elem = this.dataBase.find((item: any) => item['id'] === parseClient.id);
    if (!elem) {
      this.dataBase.push(parseClient);
    } else {
      this.dataBase[index] = parseClient;
    }
    this.dataBase = this.dataBase.filter(item => new Date().getTime() - item.time < 3000);
    this.dataBase.sort((a, b) => a.id > b.id ? 1 : -1);
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }

}

interface IClients {
  id: number;
  fio: string;
  phone: string;
  email: string;
  time?: number;
}

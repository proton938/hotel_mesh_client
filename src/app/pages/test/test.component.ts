import { Component, NgZone, OnInit } from '@angular/core';
import { BASIC_SOURCE } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { catchError, delay, map, retry } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  testBuffer = 0;

  constructor(
    private zone: NgZone,
  ) { }

  ngOnInit(): void {
    this.getTest()
      .pipe(
        map((res: any) => this.handleEvent(res)),
        delay(1500),
        retry(Infinity),
        catchError(() => this.handleError()),
      )
      .subscribe();
  }

  private handleEvent(res: number) {
    console.log(res);
    if (this.testBuffer !== res) {
      alert('изменение');
      this.testBuffer = res;
    }
  }

  private handleError() {
    return EMPTY;
  }

  private getTest(): Observable<any> {
    return Observable.create(
      (observer: any) => {
        let source = new EventSource(BASIC_SOURCE + '/test');
        source.onmessage = event => {
          this.zone.run(() => {
            observer.next(event.data);
          })
        };
        source.onerror = event => {
          this.zone.run(() => {
            source.close();
            observer.error(event);
          })
        }
      }
    );
  }

}

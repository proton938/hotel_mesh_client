import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription, throwError } from 'rxjs';
import { ClientsStreamService } from "./shared/rest/clients-stream.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'web_flux2';
  private sub: Subscription | undefined;
  dataBase: any[] = [];

  constructor(
    private zone: NgZone,
    private http: HttpClient,
    private clientsStreamService: ClientsStreamService,
  ) {
  }

  private allUsers(): Observable<any> {

    return Observable.create(
        (observer: any) => {

        let source = new EventSource('http://localhost:8080/request/allUsers');

        source.onmessage = event => {
          this.zone.run(() => {
            observer.next(event.data);
          })
        };

        source.onerror = event => {
            this.zone.run(() => {
              observer.error(event);
            })
          }
      }
    );
  }

  ngOnInit(): void {
    // this.allClients();

    /*
    this.allUsers()
      .subscribe((user: string) => this.handleResponseGetClients(user));
     */

    /*
    this.sub = this.clientsStreamService.getClientsStream()
      .subscribe(
        (client: string) => this.handleResponseGetClients(client),
        err => throwError(err)
      );
     */
  }

  private handleResponseGetClients(client: string) {
    const parseClient: IClients = JSON.parse(client);
    parseClient.time = new Date().getTime();
    let index: number = this.dataBase.findIndex((item: any) => item['id'] === parseClient.id);
    const elem = this.dataBase.find((item: any) => item['id'] === parseClient.id);
    if (!elem) {
      this.dataBase.push(parseClient);
    } else {
      this.dataBase[index] = parseClient;
    }
    this.dataBase = this.dataBase.filter(item => new Date().getTime() - item.time < 3000);
    this.dataBase.sort((a, b) => a.id > b.id ? 1 : -1);
  }

  ngOnDestroy(): void {
    this.sub && this.sub.unsubscribe();
  }

  private allClients() {
    this.http.get('http://localhost:8080/request/allClients')
      .subscribe(
        clients => {
          console.log(clients);
        },
        error => console.log(error)
      )
  }

}

interface IClients {
  id: number;
  fio: string;
  phone: string;
  email: string;
  time?: number;
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { TestComponent } from './pages/test/test.component';
import { ClientsComponent } from './pages/clients/clients.component';
import { CalendarLineComponent } from './shared/block-components/calendar-line/calendar-line.component';
import { ReceivingNotificationsComponent } from './shared/block-components/receiving-notifications/receiving-notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ClientsComponent,
    CalendarLineComponent,
    ReceivingNotificationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './pages/test/test.component';
import { AppComponent } from "./app.component";
import { ClientsComponent } from "./pages/clients/clients.component";
import { ReceivingNotificationsComponent } from "./shared/block-components/receiving-notifications/receiving-notifications.component";

const routes: Routes = [
  { path: "", component: AppComponent },
  { path: "test", component: TestComponent },
  { path: "clients", component: ClientsComponent },
  { path: "notifications", component: ReceivingNotificationsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
